package aef.sepomex.model;

import java.util.List;
import org.sql2o.Connection;
import aef.sepomex.model.pojo.Sepomex;

import static aef.sepomex.util.SepomexSQL.*;
import static aef.sepomex.util.SepomexConstants.*;
import static aef.sepomex.util.DatabaseConstants.*;
import static aef.sepomex.util.DatabaseProcessor.*;

/**
 * 
 * @since 19/09/2017
 * @author fzarate
 */
public class SepomexDaoImpl implements SepomexDao{

    @Override
    public List<Sepomex> obtenerColoniasCoberturaCp(String pCp){
        List<Sepomex> resp = null;
        
        try (Connection cnx = getProcessor(DS_TCH).open()) {
             resp = cnx.createQuery(SELECT_UBICACIONSUC_COBERTURA)
                       .addParameter(P_CP, pCp)
                       .executeAndFetch(Sepomex.class);
        }
        return resp;
    }

    @Override
    public List<Sepomex> obtenerColoniasPorCp(String pCp){
        List<Sepomex> resp = null;
        
        try (Connection cnx = getProcessor(DS_TCH).open()) {
             resp = cnx.createQuery(SELECT_CP_COLONIAS)
                       .addParameter(P_CP, pCp)
                       .executeAndFetch(Sepomex.class);
        }
        return resp;
    }
}
