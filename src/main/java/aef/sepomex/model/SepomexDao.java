package aef.sepomex.model;

import aef.sepomex.model.pojo.Sepomex;
import java.util.List;

/**
 * 
 * @since 19/09/2017
 * @author fzarate
 */
public interface SepomexDao {

    public List<Sepomex> obtenerColoniasCoberturaCp(String pCp);
    
    public List<Sepomex> obtenerColoniasPorCp(String pCp);
}
