package aef.sepomex.model.pojo;

/**
 *
 * @author fzarate
 */
public class Sepomex {
        
    private String claveEmpresa;
    private long cpId;
    private String cp;
    private String asentamiento;
    private String estado;
    private String municipio;
    private String ciudad;

    public String getClaveEmpresa() {
        return claveEmpresa;
    }

    public void setClaveEmpresa(String claveEmpresa) {
        this.claveEmpresa = claveEmpresa;
    }

    public long getCpId() {
        return cpId;
    }

    public void setCpId(long cpId) {
        this.cpId = cpId;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getAsentamiento() {
        return asentamiento;
    }

    public void setAsentamiento(String asentamiento) {
        this.asentamiento = asentamiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return "Sepomex{" + "claveEmpresa=" + claveEmpresa + ", cpId=" + cpId + ", cp=" + cp + ", asentamiento=" + asentamiento + ", estado=" + estado + ", municipio=" + municipio + ", ciudad=" + ciudad + '}';
    }
    
}
