package aef.sepomex.util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.sql2o.Sql2o;
import org.sql2o.quirks.OracleQuirks;

/**
 * 
 * @since 19/09/2017
 * @author fzarate
 */
public class DatabaseProcessor {
    public static Sql2o getProcessor(String dataSourceName) {
        Sql2o processor = null;
        if (null != dataSourceName || !"".equals(dataSourceName.trim())) {
            processor = new Sql2o(getDataSource(dataSourceName), new OracleQuirks());
        }
        return processor;
    }

    private static DataSource getDataSource(String dataSourceJndiName) {
        DataSource dataSource = null;
        Context ctx = null;

        if (null != dataSourceJndiName && "".equals(dataSourceJndiName)) {
            throw new IllegalArgumentException("dataSourceJndiName must not be empty.");
        }
        try {
            ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup(dataSourceJndiName.trim());
        } catch (Exception cce) {
            System.out.println(cce);
        }
        return dataSource;
    }
}
