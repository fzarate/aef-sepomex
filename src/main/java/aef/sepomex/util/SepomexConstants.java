package aef.sepomex.util;

/**
 *
 * @author fzarate
 */
public class SepomexConstants {
    
    public static final String P_CP = "pCp";
    public static final String P_URL = "http://104.154.169.20:8181/cxf/asentamientos/asentamientosService/consultarAsentamientos";
    public static final String P_CONTENT_TYPE = "application/json; charset=utf-8";

}
