package aef.sepomex.util;

/**
 *
 * @author fzarate
 */
public class SepomexSQL {
        
    public final static String SELECT_UBICACIONSUC_COBERTURA =
            "SELECT DISTINCT 19 AS claveEmpresa, CP.CPID AS cpId, CP.CP AS cp, CP.COLONIA AS asentamiento, CP.ESTADO as estado, CP.MUNICIPIO as municipio, CP.CIUDAD as ciudad\n" +
            "  FROM KEVIN.CP \n" +
            " INNER JOIN KEVIN.UBICACIONSUC UBI\n" +
            "    ON UBI.CPID = CP.CPID\n" +
            "   AND UBI.ESTATUS = 'A'\n" +
            " WHERE CP = :pCp\n" +
            "   AND CP.ESTATUS = 'A'\n" +
            " ORDER BY CP.COLONIA";
    
    public final static String SELECT_CP_COLONIAS =
            "SELECT DISTINCT CP.CPID AS cpId, CP.CP AS cp, CP.COLONIA AS asentamiento, CP.ESTADO as estado, CP.MUNICIPIO as municipio, CP.CIUDAD as ciudad\n" +
            "  FROM KEVIN.CP \n" +
            " WHERE CP = :pCp\n" +
            "   AND ESTATUS = 'A'\n" +
            " ORDER BY CP.COLONIA";

}
