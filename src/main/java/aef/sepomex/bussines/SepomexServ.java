package aef.sepomex.bussines;

import aef.sepomex.model.pojo.Sepomex;
import java.util.List;

/**
 * 
 * @since 19/09/2017
 * @author fzarate
 */
public interface SepomexServ {

    public List<Sepomex> obtenerCoberturaPorCP(String pCp);
    
    public List<Sepomex> obtenerColoniasPorCP(String pCp);
    
    public String obtenerCoberturaPorCpFISA(String pCp);
}
