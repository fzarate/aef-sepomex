package aef.sepomex.bussines;

import aef.sepomex.model.SepomexDao;
import aef.sepomex.model.SepomexDaoImpl;
import aef.sepomex.model.pojo.Sepomex;
import aef.sepomex.util.bussines.Http;
import static aef.sepomex.util.SepomexConstants.*;
import java.util.List;

/**
 * 
 * @since 19/09/2017
 * @author fzarate
 */
public class SepomexServImpl implements SepomexServ{

    SepomexDao spm = new SepomexDaoImpl();
    
    @Override
    public List<Sepomex> obtenerCoberturaPorCP(String pCp){
    
        return spm.obtenerColoniasCoberturaCp(pCp);
    }
    
    @Override
    public List<Sepomex> obtenerColoniasPorCP(String pCp){
    
        return spm.obtenerColoniasPorCp(pCp);
    }

    @Override
    public String obtenerCoberturaPorCpFISA(String pCp) {
    
        Http http = new Http();        
        String json = "{\"claveEmpresa\":\"000100000000\", \"cp\":\""+pCp+"\"}";
        return http.enviaPOST(P_URL, json, P_CONTENT_TYPE);
    }
}
