package aef.sepomex.service;

import aef.sepomex.bussines.SepomexServ;
import aef.sepomex.bussines.SepomexServImpl;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * 
 * @since 19/09/2017
 * @author fzarate
 */

@Path("/direccion")
public class SepomexREST {

    SepomexServ spm = new SepomexServImpl();
    
    @GET
    @Path("/cobertura_sucursales")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerCobertura(@QueryParam("cp") String pCp,
                                     @Context HttpServletRequest req) {
        
        return Response.status(200).entity(spm.obtenerCoberturaPorCP(pCp)).build();
    }
    @GET
    @Path("/fisa/cobertura_sucursales")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerCoberturaFISA(@QueryParam("cp") String pCp,
                                         @Context HttpServletRequest req) {
        return Response.status(200).entity(spm.obtenerCoberturaPorCpFISA(pCp)).build();
    }
    
    @GET
    @Path("/colonias")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerColonias(@QueryParam("cp") String pCp,
                                    @Context HttpServletRequest req) {
        
        return Response.status(200).entity(spm.obtenerColoniasPorCP(pCp)).build();
    }

}
